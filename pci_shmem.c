
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/proc_fs.h>
#include <linux/smp_lock.h>
#include <asm/uaccess.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/sysfs.h>


#define DRV_NAME   "simnow-pci-shmem"
#define DEV_NAME   "simnow-shmem"


#define SHMEM_INFO(fmt, arg...)  printk (KERN_INFO "SimNow ShMem: " fmt "\n" , ## arg)
#define SHMEM_ERR(fmt, arg...)   printk (KERN_ERR "SimNow ShMem - %s: " fmt "\n" , __func__ , ## arg)
#define SHMEM_DBG(fmt, arg...)   pr_debug ("SimNow ShMem - %s: " fmt "\n" , __func__ , ## arg)


typedef struct simnow_shmem_device {
	struct pci_dev    *dev;
	/*  for BAR0 - registers area  */
	void __iomem      *regs;
	unsigned int      regaddr;
	unsigned int      reg_size;

	/*  for operation management  */
	struct mutex      ops_lock;
} simnow_shmem_device_t;



#define DWORD                uint32_t
#define SHMEM_SIZE_MEM_REG   256   // byte
#define REG_SIZE             (sizeof(DWORD))
#define SHMEM_NUM_REGS       (SHMEM_SIZE_MEM_REG / sizeof(DWORD))
#define REG2BASEADDR(x)      ((x) * sizeof(DWORD))

#define is_valid_reg(x)      ((x) < 0 && (x) > SHMEM_NUM_REGS) ? -1 : 0

/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BASIC FUNCTIONS                            |
 * |__________________________________________________________________________|
 */

static void shmem_reg_read (simnow_shmem_device_t *shmem_dev,
		DWORD reg, DWORD *pdata) {

	*pdata = (DWORD)readl(shmem_dev->regs + REG2BASEADDR(reg));
}


static void shmem_reg_write (simnow_shmem_device_t *shmem_dev,
		DWORD reg, DWORD data){

	writel(data, shmem_dev->regs + REG2BASEADDR(reg));
}


/*  __________________________________________________________________________
 * |                                                                          |
 * |                               SYSFS INTEFACE                             |
 * |__________________________________________________________________________|
 */

static ssize_t sys_shmem_reg_show (struct device *dev,
		struct device_attribute *attr, char *buf) {

	DWORD                          reg;
	DWORD                      reg_val;
	char                      *end_str;
	struct platform_device       *pdev = to_platform_device (dev);
	simnow_shmem_device_t   *shmem_dev = platform_get_drvdata (pdev);

	if ( !shmem_dev  )
		return -EINVAL;

	reg = (DWORD)simple_strtoul (buf, &end_str, 16);
	if ( reg < 0 && reg > SHMEM_NUM_REGS )
		return -EINVAL;

	mutex_lock (&shmem_dev->ops_lock);
	shmem_reg_read (shmem_dev, reg, &reg_val);
	mutex_unlock (&shmem_dev->ops_lock);

	return sprintf (buf, "%#04X\n", reg_val);
}


static ssize_t sys_shmem_reg_store (struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count) {

	DWORD                          reg;
	DWORD                      reg_val;
	unsigned long                value;
	char                        *start = (char *)buf;
	struct platform_device       *pdev = to_platform_device (dev);
	simnow_shmem_device_t   *shmem_dev = platform_get_drvdata (pdev);

	if ( !shmem_dev  )
		return -EINVAL;

	while ( *start == ' ' )
		start++;

	reg = (DWORD)simple_strtoul (start, &start, 16);
	if ( is_valid_reg (reg) ) {
		return -EINVAL;
	}

	while (*start == ' ')
		start++;

	if (strict_strtoul (start, 16, &value) ) {
		return -EINVAL;
	}
	reg_val = (DWORD)value;

	mutex_lock (&shmem_dev->ops_lock);
	shmem_reg_write (shmem_dev, reg, reg_val);
	mutex_unlock (&shmem_dev->ops_lock);

	return count;
}


static ssize_t sys_shmem_dump_reg_show (struct device *dev,
		struct device_attribute *attr, char *buf) {

	DWORD                          reg;
	DWORD                      reg_val;
	int                        regsize,
							  wordsize;
	unsigned char                 *msg;
	unsigned char                 *tmp;
	char                      *end_str;
	struct platform_device       *pdev = to_platform_device (dev);
	simnow_shmem_device_t   *shmem_dev = platform_get_drvdata (pdev);


	if ( !shmem_dev  )
		return -EINVAL;

	regsize = ((REG_SIZE * 2) + 2) * 2 + 1 + 2 + 1;
	wordsize = regsize * SHMEM_NUM_REGS;
	tmp = (unsigned char *)kzalloc (sizeof(char) * regsize, GFP_KERNEL);
	msg = (unsigned char *)kzalloc (sizeof(char) * wordsize, GFP_KERNEL);

	mutex_lock (&shmem_dev->ops_lock);
	for ( reg = 0 ; reg < SHMEM_NUM_REGS ; reg++ ) {
		shmem_reg_read (shmem_dev, reg, &reg_val);
		sprintf (tmp, "%#08X : %#08X\n", reg, reg_val);
		strcat (msg, tmp);
	}
	mutex_unlock (&shmem_dev->ops_lock);

	return sprintf (buf, "%s\n", msg);
}


static DEVICE_ATTR (reg, 0x666, sys_shmem_reg_show, sys_shmem_reg_store);
static DEVICE_ATTR (dump_reg, 0x444, sys_shmem_dump_reg_show, NULL);


static struct attribute *simnow_shmem_attrs[] = {
	&dev_attr_reg.attr,
	&dev_attr_dump_reg.attr,
	NULL,	/*  sentinel  */
};


static struct attribute_group simnow_shmem_attr_group = {
	.attrs = simnow_shmem_attrs,
};



/*  __________________________________________________________________________
 * |                                                                          |
 * |                              DRIVER INTEFACE                             |
 * |__________________________________________________________________________|
 */

static int simnow_shmem_probe_device (struct pci_dev *pdev,
					const struct pci_device_id * ent) {

	int result, ret;
	simnow_shmem_device_t  *simnow_shmem_dev;

	simnow_shmem_dev = kzalloc (sizeof(simnow_shmem_device_t), GFP_KERNEL);
	if ( simnow_shmem_dev == NULL ) {
		SHMEM_ERR ("failed to allocate state");
		return -ENOMEM;
	}

	SHMEM_INFO ("Probing for Simnow Share Memory device");

	result = pci_enable_device (pdev);
	if (result) {
		SHMEM_ERR ("Cannot probe Simnow Shared Memory device  %s: error %d",
			pci_name(pdev), result);
		return result;
	}

	result = pci_request_regions (pdev, DEV_NAME);
	if ( result < 0 ) {
		SHMEM_ERR ("cannot request regions");
		goto pci_disable;
	}

	/*  BAR0 mapping... */
	simnow_shmem_dev->regaddr  =  pci_resource_start(pdev, 0);
	simnow_shmem_dev->reg_size = pci_resource_len(pdev, 0);
	simnow_shmem_dev->regs     = pci_iomap(pdev, 0, 0x100);

	simnow_shmem_dev->dev = pdev;

	if ( !simnow_shmem_dev->regs ) {
		SHMEM_ERR ("cannot ioremap registers of size %d byte", 0x100);
		goto reg_release;
	}

	mutex_init (&simnow_shmem_dev->ops_lock);
	platform_set_drvdata (pdev, simnow_shmem_dev);

	ret = sysfs_create_group (&pdev->dev.kobj, &simnow_shmem_attr_group);
	if ( ret )
		SHMEM_ERR ("cannot create sysfs items");

	SHMEM_INFO ("probe done...");
	return 0;

reg_release:
	pci_release_regions (pdev);
pci_disable:
	pci_disable_device (pdev);
	kfree (simnow_shmem_dev);
	return -EBUSY;

}


static void simnow_shmem_remove_device (struct pci_dev* pdev) {
	simnow_shmem_device_t   *shmem_dev = platform_get_drvdata (pdev);
	SHMEM_INFO ("Unregister device.");
	pci_iounmap (pdev, shmem_dev->regs);
	pci_release_regions (pdev);
	sysfs_remove_group (&pdev->dev.kobj, &simnow_shmem_attr_group);
	pci_disable_device (pdev);
	kfree (shmem_dev);
}


static struct pci_device_id simnow_shmem_id_table[] = {
        { 0x3af1, 0x3310, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
        { 0 },
};
MODULE_DEVICE_TABLE (pci, simnow_shmem_id_table);


static struct pci_driver simnow_shmem_driver = {
	.name	        = DRV_NAME,
	.id_table       = simnow_shmem_id_table,
	.probe          = simnow_shmem_probe_device,
	.remove	        = simnow_shmem_remove_device,
};


static int __init simnow_shmem_init_module (void) {
	int err = 0;

	err = pci_register_driver (&simnow_shmem_driver);
	if (err < 0) {
		goto error;
	}

	return 0;

error:
	return err;
}


static void __exit simnow_shmem_cleanup_module (void) {
		pci_unregister_driver (&simnow_shmem_driver);
}


module_init(simnow_shmem_init_module);
module_exit(simnow_shmem_cleanup_module);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Davide Cardillo <davide.cardillo85@gmail.com>");
MODULE_DESCRIPTION("Simnow PCI shared memory module");
MODULE_VERSION("1.0");

