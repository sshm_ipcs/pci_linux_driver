#! /bin/bash

echo "get module with xget pcidriver/pci_shmem.ko pci_shmem.ko"
xget pcidriver/pci_shmem.ko pci_shmem.ko
ls -l
is_loaded=$(cat /proc/modules | grep pci_shmem)
if [ "${is_loaded}" != "" ]; then
	echo "unload module"
	rmmod pci_shmem
else
	echo "module not loaded"
fi
echo "load new module"
insmod pci_shmem.ko
lsmod

