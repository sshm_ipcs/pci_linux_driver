

obj-m := pci_shmem.o

KDIR   := /lib/modules/$(shell uname -r)/build
PWD    := $(shell pwd)
SIMNOW := /opt/arc/simnow-linux64-4.7.4nda

all: modules

modules:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

modules_install: modules
		cp pci_shmem.ko /opt/arc/simnow-linux64-4.7.4nda

clean:
	rm -f *.ko *.o pci_shmem.mod.c Module.symvers

